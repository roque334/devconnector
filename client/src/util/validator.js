export const checkValidity = (value, rules) => {
  let isValid = true;
  if (rules) {
    if (rules.hasOwnProperty('required') && rules.required) {
      isValid = isValid && ((typeof(value) === "string" && value.trim() !== '') || (typeof(value) === "object" && value.length > 0));
    }
    if (rules.hasOwnProperty('minLength') && rules.minLength) {
      isValid = isValid && value.trim().length >= rules.minLength;
    }
    if (rules.hasOwnProperty('isEmail') && rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = isValid && pattern.test(value);
    }
  }
  return isValid;
};
