import axios from 'axios';

import * as actionTypes from './types';
import { alert } from './alert';
import { clearProfile } from "./profile";
import setAuthToken from '../util/setAuthToken';

const beginAction = () => {
  return {
    type: actionTypes.BEGIN_AUTH_ACTION,
  };
}

const endAction = () => {
  return {
    type: actionTypes.END_AUTH_ACTION,
  };
}

const registerSuccess = (data) => {
  return {
    type: actionTypes.REGISTER_SUCCESS,
    payload: data,
  };
};

const registerFail = () => {
  return {
    type: actionTypes.REGISTER_FAIL,
  };
};

//Register User
export const register = ({ name, email, password }) => {
  return async (dispatch) => {
    dispatch(beginAction());
    const config = { headers: { 'Content-Type': 'application/json' } };
    const body = JSON.stringify({ name, email, password });

    try {
      const res = await axios.post('/api/users', body, config);
      dispatch(registerSuccess(res.data));
      dispatch(getUser());
    } catch (err) {
      const errors = err.response.data.errors;

      if (errors) {
        errors.forEach((error) => {
          dispatch(alert(error.message, 'danger'));
        });
      }

      dispatch(registerFail());
    }
    dispatch(endAction());
  };
};

const getUserSuccess = (data) => {
  return {
    type: actionTypes.GET_USER_SUCCESS,
    payload: data,
  };
};

const getUserFail = () => {
  return {
    type: actionTypes.GET_USER_FAIL,
  };
};

export const getUser = () => {
  return async (dispatch) => {
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }

    try {
      const res = await axios.get('/api/auth');
      dispatch(getUserSuccess(res.data));
    } catch (err) {
      dispatch(getUserFail());
    }
  };
};

const loginSuccess = (data) => {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    payload: data,
  };
};

const loginFail = () => {
  return {
    type: actionTypes.LOGIN_FAIL,
  };
};

export const login = ({ email, password }) => {
  return async (dispatch) => {
    dispatch(beginAction());
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const body = JSON.stringify({ email, password });

    try {
      const res = await axios.post('/api/auth', body, config);
      dispatch(loginSuccess(res.data));
      dispatch(getUser());
    } catch (err) {
      const errors = err.response.data.errors;

      errors.forEach((error) => {
        dispatch(alert(error.message, 'danger'));
      });
      dispatch(loginFail());
    }
    dispatch(endAction());
  };
};

const logoutSuccess = () => {
  return {
    type: actionTypes.LOGOUT,
  };
};

export const logout = () => {
  return (dispatch) => {
    dispatch(beginAction());
    dispatch(clearProfile());
    dispatch(logoutSuccess());
    dispatch(endAction());
  };
};
