import axios from 'axios';
import { alert } from './alert';
import { logout } from './auth';

import * as actionTypes from './types';

const beginAction = () => {
  return {
    type: actionTypes.BEGIN_PROFILE_ACTION,
  };
};

const endAction = () => {
  return {
    type: actionTypes.END_PROFILE_ACTION,
  };
};

const getProfileSuccess = (data) => {
  return {
    type: actionTypes.GET_PROFILE_SUCCESS,
    payload: data,
  };
};

const getProfileFail = (data) => {
  return {
    type: actionTypes.GET_PROFILE_FAIL,
    payload: data,
  };
};

export const getCurrentProfile = () => {
  return async (dispatch) => {
    try {
      dispatch(beginAction());
      const res = await axios.get('/api/profile/me');
      dispatch(getProfileSuccess(res.data));
    } catch (err) {
      dispatch(
        getProfileFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
    dispatch(endAction());
  };
};

const getProfilesSuccess = (data) => {
  return {
    type: actionTypes.GET_PROFILES_SUCCESS,
    payload: data,
  };
};

const getProfilesFail = (data) => {
  return {
    type: actionTypes.GET_PROFILES_FAIL,
    payload: data,
  };
};

export const getProfiles = () => {
  return async (dispatch) => {
    dispatch(beginAction());
    dispatch(clearProfileSuccess());
    try {
      const res = await axios.get('/api/profile');
      dispatch(getProfilesSuccess(res.data));
    } catch (err) {
      dispatch(
        getProfilesFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
    dispatch(endAction());
  };
};

export const getProfileById = (userId) => {
  return async (dispatch) => {
    dispatch(beginAction());
    try {
      const res = await axios.get(`/api/profile/user/${userId}`);
      dispatch(getProfileSuccess(res.data));
    } catch (err) {
      dispatch(
        getProfileFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
    dispatch(endAction());
  };
};

const clearProfileSuccess = () => {
  return {
    type: actionTypes.CLEAR_PROFILE,
  };
};

export const clearProfile = () => {
  return (dispatch) => {
    dispatch(beginAction());
    dispatch(clearProfileSuccess());
    dispatch(endAction());
  };
};

const createProfileSuccess = (data) => {
  return {
    type: actionTypes.CREATE_PROFILE_SUCCESS,
    payload: data,
  };
};

const createProfileFail = (data) => {
  return {
    type: actionTypes.CREATE_PROFILE_FAIL,
    payload: data,
  };
};

export const createProfile = (formData, history, edit = false) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const res = await axios.post('/api/profile', formData, config);
      dispatch(createProfileSuccess(res.data));
      dispatch(alert(edit ? 'Profile Updated' : 'Profile Created', 'success'));
      if (!edit) {
        history.push('/dashboard');
      }
    } catch (err) {
      const errors = err.response.data.errors;

      if (errors) {
        errors.forEach((error) => {
          dispatch(alert(error.message, 'danger'));
        });
      }

      dispatch(
        createProfileFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

export const deleteProfile = () => {
  return async (dispatch) => {
    if (window.confirm('Are you sure? This CANNOT be undone!')) {
      try {
        await axios.delete('/api/profile');
        dispatch(clearProfileSuccess());
        dispatch(logout());
        dispatch(alert('You account has been permanently deleted', 'dark'));
      } catch (err) {
        const errors = err.response.data.errors;
        if (errors) {
          errors.forEach((error) => {
            dispatch(alert(error.message, 'danger'));
          });
        }
      }
    }
  };
};

const createExperienceSuccess = (data) => {
  return {
    type: actionTypes.CREATE_EXPERIENCE_SUCCESS,
    payload: data,
  };
};

const createExperienceFail = (data) => {
  return {
    type: actionTypes.CREATE_EXPERIENCE_FAIL,
    payload: data,
  };
};

export const createExperience = (formData, history, edit = false) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      let res = null;

      if (!edit) {
        res = await axios.put('/api/profile/experience', formData, config);
      } else {
        res = await axios.put(
          `/api/profile/experience/${formData._id}`,
          formData,
          config
        );
      }

      dispatch(createExperienceSuccess(res.data));
      dispatch(
        alert(edit ? 'Experience Updated' : 'Experience Created', 'success')
      );
      history.push('/dashboard');
    } catch (err) {
      const errors = err.response.data.errors;

      if (errors) {
        errors.forEach((error) => {
          dispatch(alert(error.message, 'danger'));
        });
      }

      dispatch(
        createExperienceFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

const deleteExperienceSuccess = (data) => {
  return {
    type: actionTypes.DELETE_EXPERIENCE,
    payload: data,
  };
};

export const deleteExperience = (experienceID) => {
  return async (dispatch) => {
    try {
      const res = await axios.delete(`/api/profile/experience/${experienceID}`);
      dispatch(deleteExperienceSuccess(res.data));
      dispatch(alert('Experience Deleted'));
    } catch (err) {
      const errors = err.response.data.errors;

      if (errors) {
        errors.forEach((error) => {
          dispatch(alert(error.message, 'danger'));
        });
      }
    }
  };
};

const createEducationSuccess = (data) => {
  return {
    type: actionTypes.CREATE_EDUCATION_SUCCESS,
    payload: data,
  };
};

const createEducationFail = (data) => {
  return {
    type: actionTypes.CREATE_EDUCATION_FAIL,
    payload: data,
  };
};

export const createEducation = (formData, history, edit = false) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      let res = null;

      if (!edit) {
        res = await axios.put('/api/profile/education', formData, config);
      } else {
        res = await axios.put(
          `/api/profile/education/${formData._id}`,
          formData,
          config
        );
      }

      dispatch(createEducationSuccess(res.data));
      dispatch(
        alert(edit ? 'Education Updated' : 'Education Created', 'success')
      );
      history.push('/dashboard');
    } catch (err) {
      const errors = err.response.data.errors;

      if (errors) {
        errors.forEach((error) => {
          dispatch(alert(error.message, 'danger'));
        });
      }

      dispatch(
        createEducationFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

const deleteEducationSuccess = (data) => {
  return {
    type: actionTypes.DELETE_EDUCATION,
    payload: data,
  };
};

export const deleteEducation = (educationID) => {
  return async (dispatch) => {
    try {
      const res = await axios.delete(`/api/profile/education/${educationID}`);
      dispatch(deleteEducationSuccess(res.data));
      dispatch(alert('Education Deleted'));
    } catch (err) {
      const errors = err.response.data.errors;

      if (errors) {
        errors.forEach((error) => {
          dispatch(alert(error.message, 'danger'));
        });
      }
    }
  };
};
