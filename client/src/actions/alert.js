import { SET_ALERT, REMOVE_ALERT } from './types';
import uuid from 'uuid';

const setAlert = (id, message, alertType) => {
  return {
    type: SET_ALERT,
    payload: {
      id: id,
      message: message,
      alertType: alertType,
    },
  };
};

const removeAlert = (id) => {
  return {
    type: REMOVE_ALERT,
    payload: { id: id },
  };
};

export const alert = (message, alertType, timeout = 5000) => {
  return (dispatch) => {
    const id = uuid.v4();
    dispatch(setAlert(id, message, alertType));
    setTimeout(() => {
      dispatch(removeAlert(id));
    }, timeout);
  };
};
