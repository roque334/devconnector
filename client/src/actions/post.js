import axios from 'axios';
import * as actionTypes from './types';
import { alert } from './alert';

const beginAction = () => {
    return {
        type: actionTypes.BEGIN_POSTS_ACTION,
    }
}

const endAction = () => {
    return {
        type: actionTypes.END_POSTS_ACTION,
    }
}

const getPostsSuccess = (data) => {
  return {
    type: actionTypes.GET_POSTS_SUCCESS,
    payload: data,
  };
};

const getPostsFail = (data) => {
  return {
    type: actionTypes.GET_POSTS_FAIL,
    payload: data,
  };
};

export const getPosts = () => {
  return async (dispatch) => {
    dispatch(beginAction());
    try {
      const res = await axios.get('/api/posts');
      dispatch(getPostsSuccess(res.data));
    } catch (err) {
      dispatch(
        getPostsFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
    dispatch(endAction());
  };
};

const addLikeSuccess = (data) => {
  return {
    type: actionTypes.ADD_LIKE_SUCCESS,
    payload: data,
  };
};

export const addLike = (postId) => {
  return async (dispatch) => {
    try {
      const res = await axios.put(`/api/posts/like/${postId}`);
      dispatch(addLikeSuccess({id: postId, likes: res.data}));
    } catch (err) {
      dispatch(
        getPostsFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

const removeLikeSuccess = (data) => {
  return {
    type: actionTypes.REMOVE_LIKE_SUCCESS,
    payload: data,
  };
};

export const removeLike = (postId) => {
  return async (dispatch) => {
    try {
      const res = await axios.put(`/api/posts/unlike/${postId}`);
      dispatch(removeLikeSuccess({id: postId, likes: res.data}));
    } catch (err) {
      dispatch(
        getPostsFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

const deletePostSuccess = (data) => {
  return {
    type: actionTypes.DELETE_POST_SUCCESS,
    payload: data,
  };
};

const deletePostFail = (data) => {
  return {
    type: actionTypes.DELETE_POST_FAIL,
    payload: data,
  };
};

export const deletePost = (postId) => {
  return async (dispatch) => {
    try {
      await axios.delete(`/api/posts/${postId}`);
      dispatch(deletePostSuccess(postId));
      alert("Post Removed", "success");
    } catch (err) {
      dispatch(
        deletePostFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

const createPostSuccess = (data) => {
  return {
    type: actionTypes.CREATE_POST_SUCCESS,
    payload: data,
  };
};

const createPostFail = (data) => {
  return {
    type: actionTypes.CREATE_POST_FAIL,
    payload: data,
  };
};

export const createPost = (formData) => {
  return async (dispatch) => {
    const config = {
      header: {
        'Content-Type': 'application/json',
      }
    };

    try {
      const res = await axios.post("api/posts", formData, config);
      dispatch(createPostSuccess(res.data));
      alert("Post Created", "success");
    } catch (err) {
      dispatch(
         createPostFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

const getPostSuccess = (data) => {
  return {
    type: actionTypes.GET_POST_SUCCESS,
    payload: data,
  };
};

const getPostFail = (data) => {
  return {
    type: actionTypes.GET_POST_FAIL,
    payload: data,
  };
};

export const getPost = (postId) => {
  return async (dispatch) => {
    dispatch(beginAction());
    try {
      const res = await axios.get(`/api/posts/${postId}`);
      dispatch(getPostSuccess(res.data));
    } catch (err) {
      dispatch(
        getPostFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
    dispatch(endAction());
  };
};

const createCommentSuccess = (data) => {
  return {
    type: actionTypes.CREATE_COMMENT_SUCCESS,
    payload: data,
  };
};

const createCommentFail = (data) => {
  return {
    type: actionTypes.CREATE_COMMENT_FAIL,
    payload: data,
  };
};

export const createComment = (postId, comment) => {
  return async (dispatch) => {
    const config = {
      header: {
        'Content-Type': 'application/json',
      }
    };

    try {
      const res = await axios.post(`/api/posts/comment/${postId}`, comment, config);
      dispatch(createCommentSuccess(res.data));
      alert("Comment Created", "success");
    } catch (err) {
      dispatch(
        createCommentFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

const deleteCommentSuccess = (data) => {
  return {
    type: actionTypes.DELETE_COMMENT_SUCCESS,
    payload: data,
  };
};

const deleteCommentFail = (data) => {
  return {
    type: actionTypes.DELETE_COMMENT_FAIL,
    payload: data,
  };
};

export const deleteComment = (postId, commentId) => {
  return async (dispatch) => {
    try {
      const res = await axios.delete(`/api/posts/comment/${postId}/${commentId}`);
      dispatch(deleteCommentSuccess(res.data));
      alert("Comment Deleted", "success");
    } catch (err) {
      dispatch(
        deleteCommentFail({
          msg: err.response.statusText,
          status: err.response.status,
        })
      );
    }
  };
};

