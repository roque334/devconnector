import React, { Fragment } from 'react';
import styles from './Layout.module.css';

import NavBar from '../../components/Navigation/Navbar/Navbar';

const Layout = (props) => {
  return (
    <Fragment>
      <NavBar />
      <main className={styles.Content}>{props.children}</main>
    </Fragment>
  );
};

export default Layout;
