import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';
import Layout from './containers/Layout/Layout';
import Landing from './components/Landing/Landing';
import Alert from './components/UI/Alert/Alert';
import SignUp from './components/SignUp/SignUp';
import SignIn from './components/SignIn/SignIn';
import Dashboard from './components/Dashboard/Dashboard';

import * as actions from './actions';
import setAuthToken from './util/setAuthToken';
import ProfileForm from './components/ProfileForm/ProfileForm';
import Experience from './components/Dashboard/Experience/Experience';
import Education from './components/Dashboard/Education/Education';
import Profiles from './components/Profiles/Profiles';
import Profile from './components/Profile/Profile';
import Posts from './components/Posts/Posts';
import Post from './components/Posts/Post/Post';

if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = (props) => {
  const { onGetUser } = props;
  useEffect(() => {
    onGetUser();
  }, [onGetUser]);
  return (
    <Layout>
      <Route exact path='/' component={Landing} />
      <section className='container'>
        <Alert />
        <Switch>
          <Route exact path="/post/:postId" component={Post} />
          <Route exact path="/posts" component={Posts} />
          <Route exact path="/profile/:userId" component={Profile} />
          <Route exact path="/profiles" component={Profiles} />
          <Route exact path='/add-education/:educationId' component={Education} />
          <Route exact path='/add-experience/:experienceId' component={Experience} />
          <Route exact path='/add-education' component={Education} />
          <Route exact path='/add-experience' component={Experience} />
          <Route exact path='/create-profile' component={ProfileForm} />
          <Route exact path='/dashboard' component={Dashboard} />
          <Route exact path='/register' component={SignUp} />
          <Route exact path='/login' component={SignIn} />
          <Route path='/' component={Landing} />
        </Switch>
      </section>
    </Layout>
  );
};


const mapDispatchToProps = (dispatch) => {
  return {
    onGetUser: () => dispatch(actions.getUser()),
  };
};

export default connect(null, mapDispatchToProps)(App);
