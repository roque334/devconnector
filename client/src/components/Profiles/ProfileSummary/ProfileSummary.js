import React from 'react';

import styles from './ProfileSummary.module.css';
import ButtonLink from '../../UI/ButtonLink/ButtonLink';
import Skill from '../../UI/Skill/Skill';

const ProfileSummary = (props) => {
  const length = 3;
  const skillArray = props.skills.length > length ? props.skills.slice(0,length) : props.skills;
  const skills = skillArray.map((element) => {
    return <Skill key={element} name={element} iconStyles={['fas fa-check']} />;
  });

  if (props.skills.length > length) {
    skills.push(<Skill key="other" name="..." iconStyles={['fas fa-check']} />);
  }

  return (
    <div className={styles.ProfileSummary}>
      <div className={styles.Profile}>
        <img
          className={styles.Picture}
          src='http://www.gravatar.com/avatar/92c20b6ebe757e7c460888bf8075b125?s=200&r=pg&d=mm'
          alt='Profile'
        />
        <div className={styles.Personal}>
          <div className={styles.Name}>
            <p>{props.name}</p>
          </div>
          <div className={styles.Position}>
            <p>{props.position}</p>
          </div>
          <div className={styles.Location}>
            <p>{props.location}</p>
          </div>
          <ButtonLink to={`/profile/${props.userId}`} btnClass={['Primary', 'Sm']}>View Profile</ButtonLink>
        </div>
      </div>
      <div className={styles.Skills}>{skills}</div>
    </div>
  );
};

export default ProfileSummary;
