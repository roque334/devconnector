import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

import styles from './Profiles.module.css';
import Title from '../UI/Title/Title';
import Spinner from '../UI/Spinner/Spinner';
import ProfileSummary from './ProfileSummary/ProfileSummary';

const Profiles = (props) => {
  const { profile, onGetProfiles } = props;
  useEffect(() => {
    onGetProfiles();
  }, [onGetProfiles]);

  if (profile.loading && profile.profiles) {
    return <Spinner />;
  }

  const profileElements = profile.profiles.map((element) => {
    return (
      <ProfileSummary
        key={element.user._id}
        userId={element.user._id}
        name={element.user.name}
        position={element.status}
        location={element.location}
        skills={element.skills}
      />
    );
  });

  return (
    <div className={styles.Profiles}>
      <Title
        title='Posts'
        icon='fab fa-connectdevelop fa-lg'
        subtitle='Welcome to the community!'
      />
      {profileElements}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetProfiles: () => dispatch(actions.getProfiles()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profiles);
