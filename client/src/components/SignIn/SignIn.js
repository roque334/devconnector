import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import styles from './SignIn.module.css';
import Title from '../UI/Title/Title';
import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import * as actions from '../../actions';

import { checkValidity } from '../../util/validator';

const SignIn = (props) => {
  const [signInForm, setSignInForm] = useState({
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Email Address',
      },
      value: '',
      validation: {
        required: true,
        isEmail: true,
      },
      valid: false,
      touched: false,
    },
    password: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Password',
      },
      value: '',
      validation: {
        required: true,
        minLength: 6,
      },
      valid: false,
      touched: false,
    },
  });

  const [isFormValid, setIsFormValid] = useState(false);

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedSignInForm = { ...signInForm };
    const updatedElementSignInForm = { ...updatedSignInForm[inputIdenttifier] };
    updatedElementSignInForm.value = event.target.value;
    updatedElementSignInForm.valid = checkValidity(
      updatedElementSignInForm.value,
      updatedElementSignInForm.hasOwnProperty('validation')
        ? updatedElementSignInForm.validation
        : null
    );
    updatedElementSignInForm.touched = true;
    updatedSignInForm[inputIdenttifier] = updatedElementSignInForm;

    let isFormValidTmp = true;
    for (const key in updatedSignInForm) {
      if (updatedSignInForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedSignInForm[key].valid;
      }
    }

    setSignInForm(updatedSignInForm);
    setIsFormValid(isFormValidTmp);
  };

  const onSignInHandler = (event) => {
    event.preventDefault();
    props.onLogin(signInForm.email.value, signInForm.password.value);
  };

  if (props.isAuthenticated) {
    return <Redirect to="/dashboard" />
  }

  const formElementArray = [];
  for (const key in signInForm) {
    formElementArray.push({
      id: key,
      config: signInForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onSignInHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            value={element.config.value}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Primary', 'Sm']} disabled={!isFormValid}>
        Login
      </Button>
    </form>
  );

  return (
    <div className={styles.SignIn}>
      <Title
        title='Sign In'
        icon='fas fa-user fa-lg'
        subtitle='Create Your Account'
      />
      {form}
      <span>
        Don't have an account?{' '}
        <Link className={styles.Link} to='/register'>
          Sign Up
        </Link>
      </span>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: (email, password) => dispatch(actions.login({ email, password })),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
