import React from 'react';
import styles from './Navbar.module.css';

import Logo from "../../UI/Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";

const Navbar = (props) => {
  return (
    <header className={styles.Navbar}>
      <Logo />
      <NavigationItems />
    </header>
  );
};

export default Navbar;
