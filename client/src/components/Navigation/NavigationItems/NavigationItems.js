import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import styles from './NavigationItems.module.css';

import NavigationItem from './NavigationItem/NavigationItem';

import * as actions from '../../../actions';

const NavigationItems = (props) => {
  const authLinks = (
    <Fragment>
        <NavigationItem link={'/profiles'}>Developers</NavigationItem>
        <NavigationItem link={'/posts'}>Posts</NavigationItem>
        <NavigationItem link={'/'} onClick={() => {props.onLogout();}}>Logout</NavigationItem>
    </Fragment>
  );

  const guestLinks = (
    <Fragment>
      <NavigationItem link='/profiles'>Developers</NavigationItem>
      <NavigationItem link='/register'>Register</NavigationItem>
      <NavigationItem link='/login'>Login</NavigationItem>
    </Fragment>
  );

  return (
    <ul className={styles.NavigationItems}>
      {props.auth.isAuthenticated ? authLinks : guestLinks}
    </ul>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogout: () => dispatch(actions.logout()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationItems);
