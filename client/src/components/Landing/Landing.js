import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';


import styles from './Landing.module.css';
import ButtonLink from '../UI/ButtonLink/ButtonLink';
const Landing = (props) => {
  if (props.auth.isAuthenticated) {
    return <Redirect to="/dashboard" />
  }

  return (
    <section className={styles.Landing}>
      <div className={styles.Overlay}>
        <div className={styles.Inner}>
          <h1 className={styles.Title}>Developer Connector</h1>
          <p className={styles.Subtitle}>
            Create a developer profile/portfolio, share posts and get help from
            other developers
          </p>
          <div className={styles.Buttons}>
            <ButtonLink btnClass={['Primary', 'Lg']} to={"/register"}>Sign Up</ButtonLink>
            <ButtonLink btnClass={['Light', 'Lg']} to={"/login"}>Login</ButtonLink>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps, null)(Landing);
