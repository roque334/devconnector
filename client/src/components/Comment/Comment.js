import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../../actions';

import styles from './Comment.module.css';
import Button from '../UI/Button/Button';

const Comment = ({ auth, content, postId, onDeleteComment }) => {
  const { avatar, name, text, date, user } = content;
  return (
    <div className={styles.Comment}>
      <div className={styles.Avatar}>
        <Link to={`/profile/${user}`}>
          <img className={styles.Picture} src={avatar} alt='Profile' />
        </Link>
        <h4 className={styles.Name}>{name}</h4>
      </div>
      <div className={styles.Body}>
        <div className={styles.Text}>
          <p>{text}</p>
        </div>
        <div className={styles.Date}>
          <p>{`Posted on ${new Date(date).toISOString().slice(0, 10)}`}</p>
        </div>
        <div className={styles.Actions}>
          {auth.user._id === content.user ? (
            <Button
              btnClass={['Danger', 'Sm']}
              clicked={() => {
                onDeleteComment(postId, content._id);
              }}
            >
              <i className='fas fa-trash-alt fa-sm'></i>
            </Button>
          ) : null}
        </div>
      </div>
    </div>
  );
};

Comment.propTypes = {
  auth: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  postId: PropTypes.string.isRequired,
  onDeleteComment: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteComment: (postId, commentId) => dispatch(actions.deleteComment(postId, commentId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Comment);
