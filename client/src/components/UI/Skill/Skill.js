import React from "react";

import styles from "./Skill.module.css";

const Skill = (props) => {
    return (
        <div className={styles.Skill}>
            <i className={[...props.iconStyles].join(" ")}></i>
            <p>{props.name}</p>
        </div>
    );
};

export default Skill;