import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import styles from './Logo.module.css';

const Logo = ({ auth: { isAuthenticated } }) => {
  return (
    <div className={styles.Logo}>
      <Link className={styles.Link} to={isAuthenticated ? '/dashboard' : '/'}>
        <i className={'fas fa-code fa-lg'}></i>
        <div className={styles.Title}>DevConnector</div>
      </Link>
    </div>
  );
};

const mapStateToProps = (state) => {
  return { auth: state.auth };
};

export default connect(mapStateToProps, null)(Logo);
