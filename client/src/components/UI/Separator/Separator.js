import React from "react";

import styles from "./Separator.module.css";

const Separator = (props) => {
    return (
        
      <div className={styles.Separator}>
        <h3>{props.text}</h3>
    </div>
    );
}

export default Separator;