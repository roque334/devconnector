import React from 'react';
import styles from "./Alert.module.css"
import { connect } from 'react-redux';

const Alert = (props) => {
    let alerts = null;
    if (props.alerts && props.alerts.length > 0) {
        alerts = props.alerts.map(item => {
            return <div key={item.id} className={[styles.alert, styles[item.alertType]].join(" ")}>
                {item.message}
            </div>
        })
    }
  return alerts;
};

const mapStateToProps = (state) => {
  return { alerts: state.alert };
};

export default connect(mapStateToProps)(Alert);
