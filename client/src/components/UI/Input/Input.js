import React from 'react';

import styles from './Input.module.css';

const Input = (props) => {
  const inputStyles = [];
  let inputElement = null;

  if (props.touched && !props.valid) {
    inputStyles.push(styles.Invalid);
  }

  const icon = props.icon ? <i className={props.icon}></i> : null;
  const label = props.label ? (
    <p className={styles.Label}>{props.label}</p>
  ) : null;
  const note = props.note ? <small className={styles.Note}>{props.note}</small> : null;

  switch (props.elementType) {
    case 'input':
      inputStyles.unshift(styles.InputElement);
      inputElement = (
        <div className={styles.Container}>
          {label}
          <div className={styles.InputContainer}>
            {icon}
            <input
              className={inputStyles.join(' ')}
              {...props.elementConfig}
              name={props.elementId}
              value={props.value}
              onChange={props.changed}
            />
          </div>
          {note}
        </div>
      );
      break;
    case 'textarea':
      inputStyles.unshift(styles.InputElement);
      inputElement = (
        <div className={styles.Container}>
          {label}
          <div className={styles.TextAreaContainer}>
            <textarea
              cols='30'
              rows='5'
              className={inputStyles.join(' ')}
              {...props.elementConfig}
              name={props.elementId}
              value={props.value}
              onChange={props.changed}
            />
          </div>
          {note}
        </div>
      );
      break;
    case 'select':
      inputStyles.unshift(styles.InputElement);
      inputElement = (
        <div className={styles.Container}>
          {label}
          <div className={styles.SelectContainer}>
            <select
              className={inputStyles.join(' ')}
              {...props.elementConfig}
              name={props.elementId}
              value={props.value}
              onChange={props.changed}
            >
              {props.elementConfig.options.map((element) => {
                return (
                  <option key={element.value} value={element.value}>
                    {element.displayValue}
                  </option>
                );
              })}
            </select>
          </div>
          {note}
        </div>
      );
      break;
    case 'date':
      inputStyles.unshift(styles.InputElement);
      inputElement = (
        <div className={styles.Container}>
          {label}
          <div className={styles.DateContainer}>
            <input
              className={inputStyles.join(' ')}
              {...props.elementConfig}
              name={props.elementId}
              value={props.value}
              onChange={props.changed}
            />
          </div>
          {note}
        </div>
      );
      break;
    case 'checkbox':
      inputElement = (
        <div className={styles.Container}>
          <div className={styles.CheckboxContainer}>
            <input
              className={inputStyles.join(' ')}
              {...props.elementConfig}
              id={props.elementId}
              name={props.elementId}
              checked={props.value}
              onChange={props.changed}
            />
            {label}
          </div>
          {note}
        </div>
      );
      break;
    default:
      inputElement = (
        <div className={styles.Container}>
        <input
          className={inputStyles.join(' ')}
          {...props.elementConfig}
          name={props.elementId}
          value={props.value}
          onChange={props.changed}
        />
        </div>
      );
      break;
  }

  return <div className={styles.Input}>{inputElement}</div>;
};

export default Input;
