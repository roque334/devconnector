import React from 'react'
import styles from "./Spinner.module.css";

const Spinner = () => {
    return (
        <div className={styles["pixel-spinner"]}>
            <div className={styles["pixel-spinner-inner"]}></div>
        </div>
    )
}

export default Spinner;