import React from 'react'
import { Link } from 'react-router-dom';

import styles from "./ButtonLink.module.css";

const ButtonLink = (props) => {
    let tmpClasses = props.btnClass;
    tmpClasses.unshift("ButtonLink");
    let classes = tmpClasses.map((element) => {
        return styles[element];
    });

    return (
        <Link to={props.to} className={classes.join(" ")}>
            {props.children}
        </Link>
    )
}

export default ButtonLink;
