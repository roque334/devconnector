import React, { Fragment } from 'react';

import styles from './Title.module.css';

const Title = (props) => {
  return (
    <Fragment>
      <h1 className={styles.Title}>{props.title}</h1>
      <div className={styles.Subtitle}>
        <i className={props.icon}></i>
        <p className={styles.Text}>{props.subtitle}</p>
      </div>
    </Fragment>
  );
};

export default Title;
