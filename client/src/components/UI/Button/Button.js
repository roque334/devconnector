import React from "react";

import styles from "./Button.module.css";

const Button = (props) => {
    let tmpClasses = props.btnClass;
    tmpClasses.unshift("Button");
    let classes = tmpClasses.map((element) => {
        return styles[element];
    });
    
    return <button 
    className={classes.join(" ")}
    disabled={props.disabled}
    onClick={props.clicked}>{props.children}</button>
};

export default Button;