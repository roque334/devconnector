import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';

import styles from './ProfileForm.module.css';

import * as actions from '../../actions';

import { checkValidity } from '../../util/validator';
import Title from '../UI/Title/Title';
import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import Spinner from '../UI/Spinner/Spinner';
import ButtonLink from '../UI/ButtonLink/ButtonLink';

const ProfileForm = (props) => {
  const { auth, profile, onCreateProfile, history } = props;

  const [profileForm, setProfileForm] = useState({
    status: {
      elementType: 'select',
      elementConfig: {
        type: 'text',
        placeholder: '* Select Professional Status',
        options: [
          { displayValue: '* Select Professional Status', value: '' },
          { displayValue: 'Developer', value: 'developer' },
          { displayValue: 'Junior Developer', value: 'junior' },
          { displayValue: 'Senior Developer', value: 'senior' },
          { displayValue: 'Manager', value: 'manager' },
          { displayValue: 'Student or Learning', value: 'student' },
          { displayValue: 'Instructor or Teacher', value: 'instruction' },
          { displayValue: 'Intern', value: 'intern' },
          { displayValue: 'Other', value: 'other' },
        ],
      },
      value: '',
      note: 'Give us an idea of where you are at in your career',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    company: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Company',
      },
      value: '',
      note: 'Could be your own company or one you work for',
      validation: {},
      valid: true,
      touched: false,
    },
    website: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Website',
      },
      value: '',
      note: 'Could be your own or a company website',
      validation: {},
      valid: true,
      touched: false,
    },
    location: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Location',
      },
      value: '',
      note: 'City & state suggested (eg. Boston, MA)',
      validation: {},
      valid: true,
      touched: false,
    },
    skills: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '* Skills',
      },
      value: '',
      note: 'Please use comma separated values (eg. HTML,CSS,JavaScript,PHP)',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    githubusername: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Github Username',
      },
      value: '',
      note: 'Tell us a little about yourself',
      validation: {},
      valid: true,
      touched: false,
    },
    bio: {
      elementType: 'textarea',
      elementConfig: {
        type: 'text',
        placeholder: 'A short bio of yourself',
      },
      value: '',
      note: 'Give us an idea of where you are at in your career',
      validation: {},
      valid: true,
      touched: false,
    },
    twitter: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Twitter URL',
      },
      value: '',
      icon: 'fab fa-twitter fa-2x color-twitter',
      validation: {},
      valid: true,
      touched: false,
    },
    facebook: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Facebook URL',
      },
      value: '',
      icon: 'fab fa-facebook fa-2x color-facebook',
      validation: {},
      valid: true,
      touched: false,
    },
    youtube: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'YouTube URL',
      },
      value: '',
      icon: 'fab fa-youtube fa-2x color-youtube',
      validation: {},
      valid: true,
      touched: false,
    },
    linkedin: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'LinkedIn URL',
      },
      value: '',
      icon: 'fab fa-linkedin fa-2x color-linkedin',
      validation: {},
      valid: true,
      touched: false,
    },
    instagram: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Instagram URL',
      },
      value: '',
      icon: 'fab fa-instagram fa-2x color-instagram',
      validation: {},
      valid: true,
      touched: false,
    },
  });

  const [isFormValid, setIsFormValid] = useState(false);

  useEffect(() => {
    if (profile.profile) {
      const updatedProfileForm = { ...profileForm };
      for (const key in profileForm) {
        updatedProfileForm[key] = { ...updatedProfileForm[key] };
        switch (key) {
          case 'skills':
            updatedProfileForm[key].value = profile.profile[key]
              ? profile.profile[key].join()
              : '';
            break;
          default:
            updatedProfileForm[key].value = profile.profile[key]
              ? profile.profile[key]
              : '';
            break;
        }
        updatedProfileForm[key].value = profile.profile[key]
          ? profile.profile[key]
          : '';
        updatedProfileForm[key].valid = checkValidity(
          updatedProfileForm[key].value,
          updatedProfileForm[key].hasOwnProperty('validation')
            ? updatedProfileForm[key].validation
            : null
        );
      }

      for (const key in profile.profile.social) {
        updatedProfileForm[key] = { ...updatedProfileForm[key] };
        updatedProfileForm[key].value = profile.profile.social[key]
          ? profile.profile.social[key]
          : '';
        updatedProfileForm[key].valid = checkValidity(
          updatedProfileForm[key].value,
          updatedProfileForm[key].hasOwnProperty('validation')
            ? updatedProfileForm[key].validation
            : null
        );
      }

      let isFormValidTmp = true;
      for (const key in updatedProfileForm) {
        if (updatedProfileForm[key].hasOwnProperty('valid')) {
          isFormValidTmp = isFormValidTmp && updatedProfileForm[key].valid;
        }
      }
      setProfileForm(updatedProfileForm);
      setIsFormValid(isFormValidTmp);
    }
  }, [profile.profile]);

  if (!auth.isAuthenticated) {
    return <Redirect to='/login' />;
  }

  if (profile.loading && profile.profile === null) {
    return <Spinner />;
  }

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedProfileForm = { ...profileForm };
    const updatedElementProfileForm = {
      ...updatedProfileForm[inputIdenttifier],
    };
    switch (event.target.name) {
      case 'skills':
        updatedElementProfileForm.value = event.target.value.split(",");
        break;
      case 'current':
        updatedElementProfileForm.value = event.target.checked
        break;
      default:
        updatedElementProfileForm.value = event.target.value;
        break;
    }
    updatedElementProfileForm.valid = checkValidity(
      updatedElementProfileForm.value,
      updatedElementProfileForm.hasOwnProperty('validation')
        ? updatedElementProfileForm.validation
        : null
    );
    updatedElementProfileForm.touched = true;
    updatedProfileForm[inputIdenttifier] = updatedElementProfileForm;

    let isFormValidTmp = true;
    for (const key in updatedProfileForm) {
      if (updatedProfileForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedProfileForm[key].valid;
      }
    }

    setProfileForm(updatedProfileForm);
    setIsFormValid(isFormValidTmp);
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();
    const profile = {
      company: profileForm.company.value,
      website: profileForm.website.value,
      location: profileForm.location.value,
      bio: profileForm.bio.value,
      status: profileForm.status.value,
      githubusername: profileForm.githubusername.value,
      skills: profileForm.skills.value,
      youtube: profileForm.youtube.value,
      facebook: profileForm.facebook.value,
      twitter: profileForm.twitter.value,
      instagram: profileForm.instagram.value,
      linkedin: profileForm.linkedin.value,
    };

    onCreateProfile(profile, history, profile.profile !== null);
  };

  const formElementArray = [];
  for (const key in profileForm) {
    formElementArray.push({
      id: key,
      config: profileForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onSubmitHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementId={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            label={element.config.label}
            icon={element.config.icon}
            value={element.config.value}
            note={element.config.note}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Primary', 'Sm']} disabled={!isFormValid}>
        Submit Query
      </Button>
      <ButtonLink to='./dashboard' btnClass={['Light', 'Sm']}>
        Go Back
      </ButtonLink>
    </form>
  );

  return (
    <div className={styles.Profile}>
      <Title
        title='Create Your Profile'
        icon='fas fa-user fa-lg'
        subtitle="Let's get some information to make your profile stand out"
      />
      <small className={styles.Note}>* = required field</small>
      {form}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onCreateProfile: (data, history, edit) =>
      dispatch(actions.createProfile(data, history, edit)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProfileForm));
