import React, { Fragment } from 'react';
import { connect }from "react-redux";

import * as actions from "../../../actions";

import styles from './Experiences.module.css';
import Button from '../../UI/Button/Button';
import ButtonLink from '../../UI/ButtonLink/ButtonLink';

const Experiences = (props) => {

  let experiences = props.experiences.map((element) => {
    return (
      <div key={element._id} className={styles.Row}>
        <div className={[styles.Column, styles.Company].join(' ')}>
          {element.company}
        </div>
        <div className={[styles.Column, styles.Title].join(' ')}>
          {element.title}
        </div>
        <div className={[styles.Column, styles.Year].join(' ')}>
          {element.current ? new Date().toISOString().substring(0, 10) : new Date(element.to).toISOString().substring(0, 10)}
        </div>
        <div className={[styles.Column, styles.Delete].join(' ')}>
          <ButtonLink
            btnClass={['Primary', 'Sm']}
            to={`/add-experience/${element._id}`}
          >
            <i className="fas fa-pen fa-sm"></i>
          </ButtonLink>
          <Button
            btnClass={['Danger', 'Sm']}
            clicked={() => {
              props.onDeleteExperience(element._id);
            }}
          >
            <i className="fas fa-trash-alt fa-sm"></i>
          </Button>
        </div>
      </div>
    );
  });

  return (
    <Fragment>
      <h2>Experience Credentials</h2>
      <div className={styles.Table}>
        <div className={styles.Row}>
          <div
            className={`${styles.Header} ${styles.Column} ${styles.Company}`}
          >
            Company
          </div>
          <div className={`${styles.Header} ${styles.Column} ${styles.Title}`}>
            Title
          </div>
          <div className={`${styles.Header} ${styles.Column} ${styles.Year}`}>
            Year
          </div>
          <div className={`${styles.Header} ${styles.Column} ${styles.Delete}`}>
            <div className={styles.Hidden}>Delete</div>
          </div>
        </div>
        {experiences}
      </div>
    </Fragment>
  );
};


const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteExperience: (experienceID) => dispatch(actions.deleteExperience(experienceID))
  };
};

export default connect(null, mapDispatchToProps)(Experiences);
