import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';

import styles from './Dashboard.module.css';
import Spinner from '../UI/Spinner/Spinner';
import Title from '../UI/Title/Title';
import Educations from './Educations/Educations';
import Experiences from './Experiences/Experiences';
import Button from '../UI/Button/Button';
import ButtonLink from '../UI/ButtonLink/ButtonLink';

import * as actions from '../../actions';
import { Redirect } from 'react-router-dom';

const Dashboard = (props) => {
  const {auth, profile, onGetCurrentProfile, onDeleteProfile} = props;
  useEffect(() => {
    if (auth.isAuthenticated) {
      onGetCurrentProfile();
    }
  }, [auth.isAuthenticated, onGetCurrentProfile]);

  if (!auth.isAuthenticated) {
    return <Redirect to='/login' />;
  }

  if (profile.loading && profile.profile === null) {
    return <Spinner />;
  }

  let dashboard = (
    <Fragment>
      <p>You have not yet setup a profile, please add some info</p>
      <ButtonLink to='/create-profile' btnClass={['Primary', 'Sm']}>Create Profile</ButtonLink>
    </Fragment>
  );

  if (profile.profile !== null) {
    dashboard = (
      <Fragment>
        <div className={styles.Actions}>
          <div className={styles.Action}>
            <ButtonLink to="/create-profile" btnClass={['Light', 'Sm']}>
              <i className='fas fa-user-circle text-primary'></i> Edit Profile
            </ButtonLink>
          </div>
          <div className={styles.Action}>
            <ButtonLink to="/add-experience" btnClass={['Light', 'Sm']}>
              <i className='fab fa-black-tie text-primary'></i> Add Experience
            </ButtonLink>
          </div>
          <div className={styles.Action}>
            <ButtonLink to="/add-education" btnClass={['Light', 'Sm']}>
              <i className='fas fa-graduation-cap text-primary'></i> Add
              Education
            </ButtonLink>
          </div>
        </div>

        <Experiences experiences={profile.profile.experience}/>
        <Educations educations={profile.profile.education}/>
        <Button btnClass={['Danger', 'Sm']} clicked={() => {onDeleteProfile()}}>
          <i className='fas fa-user-minus'></i> Delete My Account
        </Button>
      </Fragment>
    );
  }

  return (
    <section className={styles.Dashboard}>
      <Title
        title='Dashboard'
        icon='fas fa-user fa-lg'
        subtitle={`Welcome ${auth.user && auth.user.name ?  auth.user.name : ""}`}
      />
      {dashboard}
    </section>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetCurrentProfile: () => dispatch(actions.getCurrentProfile()),
    onDeleteProfile: () => dispatch(actions.deleteProfile()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
