import React, { useState, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';

import styles from './Experience.module.css';

import * as actions from '../../../actions';

import Title from '../../UI/Title/Title';
import Spinner from '../../UI/Spinner/Spinner';
import Button from '../../UI/Button/Button';
import ButtonLink from '../../UI/ButtonLink/ButtonLink';
import Input from '../../UI/Input/Input';

import { checkValidity } from '../../../util/validator';

const Experience = (props) => {

  const [experienceForm, setExperienceForm] = useState({
    title: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '* Job Title',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    company: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '* Company',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    location: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Location',
      },
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
    from: {
      elementType: 'date',
      elementConfig: {
        type: 'date',
        placeholder: '',
      },
      label: 'From Date',
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
    current: {
      elementType: 'checkbox',
      elementConfig: {
        type: 'checkbox',
        placeholder: '',
      },
      label: 'Current Job',
      value: false,
      validation: {},
      valid: true,
      touched: false,
    },
    to: {
      elementType: 'date',
      elementConfig: {
        type: 'date',
        placeholder: '',
      },
      label: 'To Date',
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
    description: {
      elementType: 'textarea',
      elementConfig: {
        type: 'text',
        placeholder: 'Job Description',
      },
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
  });
  //2020-09-01T00:00:00.000Z

  const [loading, setLoading] = useState(false);
  const [isFormValid, setIsFormValid] = useState(false);

  const {
    auth,
    profile,
    onCreateExperience,
    match,
    history,
  } = props;
  
  const { experienceId } = match.params;
  console.log(experienceId);

  const loadExperience = useCallback(() => {
    const index = profile.profile.experience.findIndex(
      (element) => (element._id === experienceId)
    );
    console.log(index);
    const updatedExperienceForm = { ...experienceForm };
    for (const key in experienceForm) {
      updatedExperienceForm[key] = { ...updatedExperienceForm[key] };
      updatedExperienceForm[key].value = '';
      if (profile.profile.experience[index][key]) {
        switch (key) {
          case 'from':
          case 'to':
            updatedExperienceForm[key].value = new Date(profile.profile.experience[index][key]).toISOString().substring(0, 10);
            break;
          default:
            updatedExperienceForm[key].value =
              profile.profile.experience[index][key];
            break;
        }
      }
      updatedExperienceForm[key].valid = checkValidity(
        updatedExperienceForm[key].value,
        updatedExperienceForm[key].hasOwnProperty('validation')
          ? updatedExperienceForm[key].validation
          : null
      );
    }

    let isFormValidTmp = true;
    for (const key in updatedExperienceForm) {
      if (updatedExperienceForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedExperienceForm[key].valid;
      }
    }

    setExperienceForm(updatedExperienceForm);
    setIsFormValid(isFormValidTmp);
  }, [profile.profile, experienceId]);

  useEffect(() => {
    if (auth.isAuthenticated && profile.profile && experienceId) {
      setLoading(true);
      loadExperience();
      setLoading(false);
    }
  }, [auth.isAuthenticated, profile.profile, experienceId, loadExperience]);

  if (!auth.isAuthenticated) {
    return <Redirect to='/login' />;
  }

  if (loading) {
    return <Spinner />;
  }

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedExperienceForm = { ...experienceForm };
    const updatedElementExperienceForm = {
      ...updatedExperienceForm[inputIdenttifier],
    };

    if (event.target.name === 'current') {
      updatedElementExperienceForm.value = event.target.checked;
      if (event.target.checked) {
        updatedExperienceForm.to = { ...updatedExperienceForm.to };
        updatedExperienceForm.to.elementConfig = {
          ...updatedExperienceForm.to.elementConfig,
        };
        updatedExperienceForm.to.elementConfig.disabled = 'disabled';
        updatedExperienceForm.to.value = '';
      } else {
        updatedExperienceForm.to = { ...updatedExperienceForm.to };
        updatedExperienceForm.to.elementConfig = {
          ...updatedExperienceForm.to.elementConfig,
        };
        updatedExperienceForm.to.elementConfig.disabled = '';
        updatedExperienceForm.to.value = '';
      }
    } else {
      updatedElementExperienceForm.value = event.target.value;
    }

    updatedElementExperienceForm.valid = checkValidity(
      updatedElementExperienceForm.value,
      updatedElementExperienceForm.hasOwnProperty('validation')
        ? updatedElementExperienceForm.validation
        : null
    );
    updatedElementExperienceForm.touched = true;
    updatedExperienceForm[inputIdenttifier] = updatedElementExperienceForm;

    let isFormValidTmp = true;
    for (const key in updatedExperienceForm) {
      if (updatedExperienceForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedExperienceForm[key].valid;
      }
    }

    setExperienceForm(updatedExperienceForm);
    setIsFormValid(isFormValidTmp);
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();
    let edit = false;

    const experience = {
      title: experienceForm.title.value,
      company: experienceForm.company.value,
      location: experienceForm.location.value,
      from: experienceForm.from.value,
      to: experienceForm.to.value,
      current: experienceForm.current.value,
      description: experienceForm.description.value,
    };

    if (experienceId) {
      experience._id = experienceId;
      edit = true;
    }

    onCreateExperience(experience, history, edit);
  };

  const formElementArray = [];
  for (const key in experienceForm) {
    formElementArray.push({
      id: key,
      config: experienceForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onSubmitHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementId={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            label={element.config.label}
            icon={element.config.icon}
            value={element.config.value}
            note={element.config.note}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Primary', 'Sm']} disabled={!isFormValid}>
        Submit Query
      </Button>
      <ButtonLink to="/dashboard" btnClass={['Light', 'Sm']}>Go Back</ButtonLink>
    </form>
  );

  return (
    <div className={styles.Experience}>
      <Title
        title='Add An Experience'
        icon='fas fa-code-branch fa-lg'
        subtitle='Add any developer/programming positions that you have had in the past'
      />
      <small className={styles.Note}>* = required field</small>
      {form}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onCreateExperience: (data, history, edit) =>
      dispatch(actions.createExperience(data, history, edit)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Experience));
