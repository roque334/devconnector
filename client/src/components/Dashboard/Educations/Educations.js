import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import * as actions from "../../../actions";

import styles from './Educations.module.css';
import Button from '../../UI/Button/Button';
import ButtonLink from '../../UI/ButtonLink/ButtonLink';

const Educations = (props) => {

  let educations = props.educations.map((element) => {
    return (
      <div key={element._id} className={styles.Row}>
        <div className={[styles.Column, styles.School].join(' ')}>
          {element.institution}
        </div>
        <div className={[styles.Column, styles.Degree].join(' ')}>
          {element.degree}
        </div>
        <div className={[styles.Column, styles.Year].join(' ')}>
          {element.current ? new Date().toISOString().substring(0, 10) : new Date(element.to).toISOString().substring(0, 10)}
        </div>
        <div className={[styles.Column, styles.Delete].join(' ')}>
          <ButtonLink
            btnClass={['Primary', 'Sm']}
            to={`/add-education/${element._id}`}
          >
            <i className="fas fa-pen fa-sm"></i>
          </ButtonLink>
          <Button
            btnClass={['Danger', 'Sm']}
            clicked={() => {
              props.onDeleteEducation(element._id);
            }}
          >
            <i className="fas fa-trash-alt fa-sm"></i>
          </Button>
        </div>
      </div>
    );
  });

  return (
    <Fragment>
      <h2>Education Credentials</h2>
      <div className={styles.Table}>
        <div className={styles.Row}>
          <div
            className={[styles.Header, styles.Column, styles.School].join(' ')}
          >
            School
          </div>
          <div
            className={[styles.Header, styles.Column, styles.Degree].join(' ')}
          >
            Degree
          </div>
          <div
            className={[styles.Header, styles.Column, styles.Year].join(' ')}
          >
            Year
          </div>
          <div
            className={[styles.Header, styles.Column, styles.Delete].join(' ')}
          >
            <div className={styles.Hidden}>Delete</div>
          </div>
        </div>
        {educations}
      </div>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteEducation: (educationID) => dispatch(actions.deleteEducation(educationID))
  };
};

export default connect(null, mapDispatchToProps)(Educations);
