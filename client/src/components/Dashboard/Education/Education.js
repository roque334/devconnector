import React, { useState, useEffect, useCallback } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import styles from './Education.module.css';

import * as actions from '../../../actions';

import Title from '../../UI/Title/Title';
import Spinner from '../../UI/Spinner/Spinner';
import Button from '../../UI/Button/Button';
import Input from '../../UI/Input/Input';

import { checkValidity } from '../../../util/validator';
import ButtonLink from '../../UI/ButtonLink/ButtonLink';

const Education = (props) => {
  const [educationForm, setEducationForm] = useState({
    institution: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '* School or Bootcamp',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    degree: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '* Degree or Certificate',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    fieldofstudy: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Field Of Study',
      },
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
    from: {
      elementType: 'date',
      elementConfig: {
        type: 'date',
        placeholder: '',
      },
      label: 'From Date',
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
    current: {
      elementType: 'checkbox',
      elementConfig: {
        type: 'checkbox',
        placeholder: '',
      },
      label: 'Current School or Bootcamp',
      value: false,
      validation: {},
      valid: true,
      touched: false,
    },
    to: {
      elementType: 'date',
      elementConfig: {
        type: 'date',
        placeholder: '',
      },
      label: 'To Date',
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
    description: {
      elementType: 'textarea',
      elementConfig: {
        type: 'text',
        placeholder: 'Program Description',
      },
      value: '',
      validation: {},
      valid: true,
      touched: false,
    },
  });

  const [loading, setLoading] = useState(false);
  const [isFormValid, setIsFormValid] = useState(false);

  const {
    auth,
    profile,
    onCreateEducation,
    match,
    history,
  } = props;
  
  const { educationId } = match.params;

  const loadEducation = useCallback(() => {
    const index = profile.profile.education.findIndex(
      (element) => (element._id === educationId)
    );
    const updatedEducationForm = { ...educationForm };
      for (const key in educationForm) {
        updatedEducationForm[key] = { ...updatedEducationForm[key] };
        updatedEducationForm[key].value = '';
        if (profile.profile.education[index][key]) {
          switch (key) {
            case 'from':
            case 'to':
              updatedEducationForm[key].value = new Date(
                profile.profile.education[index][key]
              )
                .toISOString()
                .substring(0, 10);
              break;
            default:
              updatedEducationForm[key].value =
                profile.profile.education[index][key];
              break;
          }
        }
        updatedEducationForm[key].valid = checkValidity(
          updatedEducationForm[key].value,
          updatedEducationForm[key].hasOwnProperty('validation')
            ? updatedEducationForm[key].validation
            : null
        );
      }

      let isFormValidTmp = true;
      for (const key in updatedEducationForm) {
        if (updatedEducationForm[key].hasOwnProperty('valid')) {
          isFormValidTmp = isFormValidTmp && updatedEducationForm[key].valid;
        }
      }

      setEducationForm(updatedEducationForm);
      setIsFormValid(isFormValidTmp);
  }, [profile.profile, educationId]);

  useEffect(() => {
    if (auth.isAuthenticated && profile.profile && educationId) {
      setLoading(true);
      loadEducation();
      setLoading(false);
    }
  }, [auth.isAuthenticated, profile.profile, educationId, loadEducation]);

  if (!auth.isAuthenticated) {
    return <Redirect to='/login' />;
  }

  if (loading) {
    return <Spinner />;
  }

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedEducationForm = { ...educationForm };
    const updatedElementEducationForm = {
      ...updatedEducationForm[inputIdenttifier],
    };
    if (event.target.name === 'current') {
      updatedElementEducationForm.value = event.target.checked;
      if (event.target.checked) {
        updatedEducationForm.to = { ...updatedEducationForm.to };
        updatedEducationForm.to.elementConfig = {
          ...updatedEducationForm.to.elementConfig,
        };
        updatedEducationForm.to.elementConfig.disabled = 'disabled';
        updatedEducationForm.to.value = '';
      } else {
        updatedEducationForm.to = { ...updatedEducationForm.to };
        updatedEducationForm.to.elementConfig = {
          ...updatedEducationForm.to.elementConfig,
        };
        updatedEducationForm.to.elementConfig.disabled = '';
        updatedEducationForm.to.value = '';
      }
    } else {
      updatedElementEducationForm.value = event.target.value;
    }
    updatedElementEducationForm.valid = checkValidity(
      updatedElementEducationForm.value,
      updatedElementEducationForm.hasOwnProperty('validation')
        ? updatedElementEducationForm.validation
        : null
    );
    updatedElementEducationForm.touched = true;
    updatedEducationForm[inputIdenttifier] = updatedElementEducationForm;

    let isFormValidTmp = true;
    for (const key in updatedEducationForm) {
      if (updatedEducationForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedEducationForm[key].valid;
      }
    }

    setEducationForm(updatedEducationForm);
    setIsFormValid(isFormValidTmp);
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();
    let edit = false;

    const education = {
      institution: educationForm.institution.value,
      degree: educationForm.degree.value,
      fieldofstudy: educationForm.fieldofstudy.value,
      from: educationForm.from.value,
      to: educationForm.to.value,
      current: educationForm.current.value,
      description: educationForm.description.value,
    };

    if (educationId) {
      education._id = educationId;
      edit = true;
    }

    onCreateEducation(education, history, edit);
  };

  const formElementArray = [];
  for (const key in educationForm) {
    formElementArray.push({
      id: key,
      config: educationForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onSubmitHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementId={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            label={element.config.label}
            value={element.config.value}
            note={element.config.note}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Primary', 'Sm']} disabled={!isFormValid}>
        Submit Query
      </Button>
      <ButtonLink to='/dashboard' btnClass={['Light', 'Sm']}>
        Go Back
      </ButtonLink>
    </form>
  );

  return (
    <div className={styles.Education}>
      <Title
        title='Add Your Education'
        icon='fas fa-graduation-cap fa-lg'
        subtitle='Add any school, bootcamp, etc that you have attended'
      />
      <small className={styles.Note}>* = required field</small>
      {form}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onCreateEducation: (data, history, edit) =>
      dispatch(actions.createEducation(data, history, edit)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Education));
