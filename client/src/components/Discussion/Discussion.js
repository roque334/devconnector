import React, { useState } from 'react';

import styles from './Discussion.module.css';

import Button from '../UI/Button/Button';
import Comment from '../Comment/Comment';
import Separator from '../UI/Separator/Separator';
import Input from '../UI/Input/Input';
import { checkValidity } from '../../util/validator';

const Discussion = (props) => {
  const [postForm, setPostForm] = useState({
    text: {
      elementType: 'textarea',
      elementConfig: {
        type: 'text',
        placeholder: 'Comment on this post',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  });

  const [isFormValid, setIsFormValid] = useState(false);

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedPostForm = { ...postForm };
    const updatedElementPostForm = { ...updatedPostForm[inputIdenttifier] };
    updatedElementPostForm.value = event.target.value;
    updatedElementPostForm.valid = checkValidity(
      updatedElementPostForm.value,
      updatedElementPostForm.hasOwnProperty('validation')
        ? updatedElementPostForm.validation
        : null
    );
    updatedElementPostForm.touched = true;
    updatedPostForm[inputIdenttifier] = updatedElementPostForm;

    let isFormValidTmp = true;
    for (const key in updatedPostForm) {
      if (updatedPostForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedPostForm[key].valid;
      }
    }

    setPostForm(updatedPostForm);
    setIsFormValid(isFormValidTmp);
  };

  const onPostHandler = (event) => {
    event.PreventDefault();
  };

  const formElementArray = [];
  for (const key in postForm) {
    formElementArray.push({
      id: key,
      config: postForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onPostHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            value={element.config.value}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Dark', 'Sm']} disabled={!isFormValid}>
        Submit
      </Button>
    </form>
  );

  return (
    <div className={styles.Discussion}>
      <Button btnClass={['Light', 'Sm']}>Back To Posts</Button>
      <Comment />
      <Separator text='Leave A Comment' />
      {form}
      <Comment />
      <Comment />
    </div>
  );
};

export default Discussion;
