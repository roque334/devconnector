import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import * as actions from '../../actions';

import styles from './SignUp.module.css';
import Title from '../UI/Title/Title';
import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';

import { checkValidity } from '../../util/validator';

const SignUp = (props) => {
  const [signUpForm, setSignUpForm] = useState({
    name: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Name',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Email Address',
      },
      value: '',
      validation: {
        required: true,
        isEmail: true,
      },
      valid: false,
      touched: false,
    },
    password: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Password',
      },
      value: '',
      validation: {
        required: true,
        minLength: 6,
      },
      valid: false,
      touched: false,
    },
    confirmPassword: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Confirm Password',
      },
      value: '',
      validation: {
        required: true,
        minLength: 6,
      },
      valid: false,
      touched: false,
    },
  });

  const [isFormValid, setIsFormValid] = useState(false);

  if (props.isAuthenticated) {
    return <Redirect to="/dashboard" />
  }

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedSignUpForm = { ...signUpForm };
    const updatedElementSignUpForm = { ...updatedSignUpForm[inputIdenttifier] };
    updatedElementSignUpForm.value = event.target.value;
    updatedElementSignUpForm.valid = checkValidity(
      updatedElementSignUpForm.value,
      updatedElementSignUpForm.hasOwnProperty('validation')
        ? updatedElementSignUpForm.validation
        : null
    );
    updatedElementSignUpForm.touched = true;
    updatedSignUpForm[inputIdenttifier] = updatedElementSignUpForm;

    let isFormValidTmp = true;
    for (const key in updatedSignUpForm) {
      if (updatedSignUpForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedSignUpForm[key].valid;
      }
    }

    setSignUpForm(updatedSignUpForm);
    setIsFormValid(isFormValidTmp);
  };

  const onSignUpHandler = (event) => {
    event.preventDefault();
    if (signUpForm.password.value !== signUpForm.confirmPassword.value) {
      props.onAlert('Passport do not match', 'danger');
    } else {
      props.onAuth(
        signUpForm.name.value,
        signUpForm.email.value,
        signUpForm.password.value
      );
    }
  };
  
  if (props.isAuthenticated) {
    return <Redirect to="/dashboard" />
  }

  const formElementArray = [];
  for (const key in signUpForm) {
    formElementArray.push({
      id: key,
      config: signUpForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onSignUpHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            value={element.config.value}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Primary', 'Sm']} disabled={!isFormValid}>
        Register
      </Button>
    </form>
  );

  return (
    <div className={styles.SignUp}>
      <Title
        title='Sign Up'
        icon='fas fa-user fa-lg'
        subtitle='Create Your Account'
      />
      <small className={styles.Note}>
        This site uses Gravatar so if you want a profile image, use a Gravatar
        email
      </small>
      {form}
      <span>
        Already have an account?{' '}
        <Link className={styles.Link} to='/'>
          Sign In
        </Link>
      </span>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAlert: (message, alertType) =>
      dispatch(actions.alert(message, alertType)),
    onAuth: (name, email, password) =>
      dispatch(actions.register({ name, email, password })),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
