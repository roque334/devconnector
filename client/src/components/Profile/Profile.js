import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

import styles from './Profile.module.css';
import Spinner from '../UI/Spinner/Spinner';
import ButtonLink from '../UI/ButtonLink/ButtonLink';
import Skill from '../UI/Skill/Skill';
import Card from './Card/Card';

import Experiences from './Experiences/Experiences';
import Educations from './Educations/Educations';

const Profile = (props) => {
  const {
    auth,
    profile: { profile, loading },
    onGetProfileById,
    match: {
      params: { userId },
    },
  } = props;

  useEffect(() => {
    onGetProfileById(userId);
  }, [userId, onGetProfileById]);

  if (loading || !profile) {
    return <Spinner />;
  }

  const skills = profile.skills.map((element) => {
    return <Skill key={element} name={element} iconStyles={['fas fa-check']} />;
  });

  return (
    <div className={styles.Profile}>
      <ButtonLink to='/profiles' btnClass={['Light', 'Sm']}>
        Back To Profiles
      </ButtonLink>
      {auth.isAuthenticated && auth.user._id === profile.user._id ? (
        <ButtonLink to='/create-profile' btnClass={['Dark', 'Sm']}>
          Edit Profile
        </ButtonLink>
      ) : null}
      <Card profile={profile} />
      <div className={styles.Info}>
        { profile.bio ? (
          <Fragment>
            <h2 className={styles.Title}>{`${
              profile.user.name.trim().split(' ')[0]
            }'s Bio`}</h2>
            <p className={styles.Description}>{profile.bio}</p>
            <div className={styles.Line} />
          </Fragment>
        ) : null}

        <h2 className={styles.Title}>Skill Set</h2>
        <div className={styles.Skills}>{skills}</div>
      </div>
      <div className={styles.Details}>
        <Experiences set={profile.experience} />
        <Educations set={profile.education} />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetProfileById: (userId) => dispatch(actions.getProfileById(userId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
