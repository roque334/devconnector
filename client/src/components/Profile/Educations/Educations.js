import React, { Fragment } from 'react';

import styles from './Educations.module.css';
import Education from './Education/Education';

const Educations = (props) => {
  let educations = <h4 className={styles.Message}>No education credentials</h4>;
  if (props.set.length > 0) {
    const lastEducationsIndex = props.set.length - 1;
    educations = props.set.map((element, index) => {
    return (
      <Fragment key={element._id}>
        <Education
          institution={element.institution}
          from={element.from}
          to={element.to}
          current={element.current}
          degree={element.degree}
          fieldofstudy={element.fieldofstudy}
          description={element.description}
        />
        {index < lastEducationsIndex ? <div className={styles.Line}> </div> : null}
      </Fragment>
    );
  });
  }
  

  return (
    <div className={styles.Educations}>
      <h2 className={styles.Title}>Education</h2>
      {educations}
    </div>
  );
};

export default Educations;
