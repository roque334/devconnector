import React from 'react';

import styles from './Education.module.css';

const Education = (props) => {
  return (
    <div className={styles.Education}>
      <h3 className={styles.School}>{props.institution}</h3>
      <p className={styles.Period}>
      {new Date(props.from).toISOString().slice(0,10)} -- {props.current ? 'Now' : new Date(props.to).toISOString().slice(0,10)}
      </p>
      <p className={styles.Degree}>
        <strong>Degree: </strong>
        {props.degree}
      </p>
      <p className={styles.Field}>
        <strong>Field of Study: </strong>
        {props.fieldofstudy}
      </p>
      <p className={styles.Description}>
        <strong>Description: </strong>
        {props.description}
      </p>
    </div>
  );
};

export default Education;
