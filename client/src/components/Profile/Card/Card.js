import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './Card.module.css';

const Card = (props) => {
  const { profile } = props;

  let socialMedias = null;
  if (profile.social) {
    socialMedias = (
      <div className={styles.SocialMedias}>
        {profile.social.twitter ? (
          <a href={profile.social.twitter}>
            <i className={['fab fa-twitter fa-2x', styles.Icon].join(' ')}></i>
          </a>
        ) : (
          ''
        )}
        {profile.social.facebook ? (
          <a href={profile.social.facebook}>
            <i
              className={['fab fa-facebook-square fa-2x', styles.Icon].join(
                ' '
              )}
            ></i>
          </a>
        ) : (
          ''
        )}
        {profile.social.linkedin ? (
          <a href={profile.social.linkedin}>
            <i className={['fab fa-linkedin fa-2x', styles.Icon].join(' ')}></i>
          </a>
        ) : (
          ''
        )}
        {profile.social.youtube ? (
          <a href={profile.social.youtube}>
            <i className={['fab fa-youtube fa-2x', styles.Icon].join(' ')}></i>
          </a>
        ) : (
          ''
        )}
        {profile.social.instagram ? (
          <a href={profile.social.instagram}>
            <i
              className={['fab fa-instagram fa-2x', styles.Icon].join(' ')}
            ></i>
          </a>
        ) : (
          ''
        )}
      </div>
    );
  }

  return (
    <div className={styles.Card}>
      <img
        className={styles.Picture}
        // src='http://www.gravatar.com/avatar/92c20b6ebe757e7c460888bf8075b125?s=200&r=pg&d=mm'
        src={profile.user.avatar}
        alt='Profile'
      />
      <p className={styles.Name}>{profile.user.name}</p>
      <p className={styles.Position}>{profile.status}</p>
      <p className={styles.Location}>{profile.location}</p>
      {socialMedias}
    </div>
  );
};

Card.propTypes = {
  profile: PropTypes.object.isRequired,
};

export default Card;
