import React from 'react';

import styles from './Experience.module.css';

const Experience = (props) => {
  return (
    <div className={styles.Experience}>
      <h3 className={styles.Company}>{props.company}</h3>
      <p className={styles.Period}>
        {new Date(props.from).toISOString().slice(0,10)} -- {props.current ? 'Now' : new Date(props.to).toISOString().slice(0,10)}
      </p>
      <p className={styles.Position}>
        <strong>Position: </strong>
        {props.title}
      </p>
      <p className={styles.Description}>
        <strong>Description: </strong>
        {props.description}
      </p>
    </div>
  );
};

export default Experience;
