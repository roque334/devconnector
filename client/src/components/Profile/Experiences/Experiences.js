import React, { Fragment } from 'react';

import styles from './Experiences.module.css';
import Experience from './Experience/Experience';

const Experiences = (props) => {
  let experiences = (
    <h4 className={styles.Message}>No experience credentials</h4>
  );
  if (props.set.length > 0) {
    const lastExperiencesIndex = props.set.length - 1;
    experiences = props.set.map((element, index) => {
      return (
        <Fragment key={element._id}>
          <Experience
            company={element.company}
            from={element.from}
            to={element.to}
            current={element.current}
            title={element.title}
            description={element.description}
          />
          {index < lastExperiencesIndex ? (
            <div className={styles.Line}> </div>
          ) : null}
        </Fragment>
      );
    });
  }

  return (
    <div className={styles.Experiences}>
      <h2 className={styles.Title}>Experience</h2>
      {experiences}
    </div>
  );
};

export default Experiences;
