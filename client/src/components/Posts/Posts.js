import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../../actions';

import styles from './Posts.module.css';
import Spinner from '../UI/Spinner/Spinner';
import Title from '../UI/Title/Title';
import Button from '../UI/Button/Button';
import Input from '../UI/Input/Input';
import Separator from '../UI/Separator/Separator';

import { checkValidity } from '../../util/validator';
import PostSummary from './PostSummary/PostSummary';

const Posts = ({
  post: { posts, loading, error },
  auth: { isAuthenticated },
  onGetPosts,
  onCreatePost,
}) => {
  const [postForm, setPostForm] = useState({
    text: {
      elementType: 'textarea',
      elementConfig: {
        type: 'text',
        placeholder: 'Create a post',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  });

  const [isFormValid, setIsFormValid] = useState(false);

  useEffect(() => {
    if (isAuthenticated) {
      onGetPosts();
    }
  }, [isAuthenticated, onGetPosts]);

  if (!isAuthenticated) {
    return <Redirect to='/login' />;
  }

  if (loading) {
    return <Spinner />;
  }

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedPostForm = { ...postForm };
    const updatedElementPostForm = { ...updatedPostForm[inputIdenttifier] };
    updatedElementPostForm.value = event.target.value;
    updatedElementPostForm.valid = checkValidity(
      updatedElementPostForm.value,
      updatedElementPostForm.hasOwnProperty('validation')
        ? updatedElementPostForm.validation
        : null
    );
    updatedElementPostForm.touched = true;
    updatedPostForm[inputIdenttifier] = updatedElementPostForm;

    let isFormValidTmp = true;
    for (const key in updatedPostForm) {
      if (updatedPostForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedPostForm[key].valid;
      }
    }

    setPostForm(updatedPostForm);
    setIsFormValid(isFormValidTmp);
  };

  const onPostHandler = (event) => {
    event.preventDefault();
    
    const formData = {
      text: postForm.text.value,
    }

    onCreatePost(formData);
  };

  const formElementArray = [];
  for (const key in postForm) {
    formElementArray.push({
      id: key,
      config: postForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onPostHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            value={element.config.value}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Dark', 'Sm']} disabled={!isFormValid}>
        Submit
      </Button>
    </form>
  );

  let postsSection = null;
  if (posts && posts.length > 0) {
    postsSection = posts.map((element) => {
      return <PostSummary key={element._id} content={element} readonly={false} />;
    });
  }

  return (
    <div className={styles.Posts}>
      <Title
        title='Posts'
        icon='fas fa-user fa-lg'
        subtitle='Welcome to the community!'
      />
      <Separator text='Say Something...' />
      {form}
      {postsSection}
    </div>
  );
};

Posts.propTypes = {
  auth: PropTypes.object.isRequired,
  onGetPosts: PropTypes.func.isRequired,
  onCreatePost: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return { auth: state.auth, post: state.post };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetPosts: () => dispatch(actions.getPosts()),
    onCreatePost: (formData) => dispatch(actions.createPost(formData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
