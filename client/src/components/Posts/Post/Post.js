import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { checkValidity } from '../../../util/validator';
import * as actions from '../../../actions';

import styles from './Post.module.css';
import Spinner from '../../UI/Spinner/Spinner';
import Button from '../../UI/Button/Button';
import ButtonLink from '../../UI/ButtonLink/ButtonLink';
import Input from '../../UI/Input/Input';
import Separator from '../../UI/Separator/Separator';
import PostSummary from '../PostSummary/PostSummary';
import Comment from '../../Comment/Comment';

const Post = ({
  post: { post, loading },
  auth: { isAuthenticated },
  onGetPost,
  onCreateComment,
  match: {
    params: { postId },
  },
}) => {
  const [commentForm, setCommentForm] = useState({
    text: {
      elementType: 'textarea',
      elementConfig: {
        type: 'text',
        placeholder: 'Create a comment',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  });

  const [isFormValid, setIsFormValid] = useState(false);

  useEffect(() => {
    if (isAuthenticated && postId) {
      onGetPost(postId);
      console.log(post);
    }
  }, [postId, isAuthenticated, onGetPost]);

  if (!isAuthenticated) {
    return <Redirect to='/login' />;
  }

  if (loading || !post) {
    return <Spinner />;
  }

  const inputChangeHandler = (event, inputIdenttifier) => {
    const updatedCommentForm = { ...commentForm };
    const updatedElementCommentForm = {
      ...updatedCommentForm[inputIdenttifier],
    };
    updatedElementCommentForm.value = event.target.value;
    updatedElementCommentForm.valid = checkValidity(
      updatedElementCommentForm.value,
      updatedElementCommentForm.hasOwnProperty('validation')
        ? updatedElementCommentForm.validation
        : null
    );
    updatedElementCommentForm.touched = true;
    updatedCommentForm[inputIdenttifier] = updatedElementCommentForm;

    let isFormValidTmp = true;
    for (const key in updatedCommentForm) {
      if (updatedCommentForm[key].hasOwnProperty('valid')) {
        isFormValidTmp = isFormValidTmp && updatedCommentForm[key].valid;
      }
    }

    setCommentForm(updatedCommentForm);
    setIsFormValid(isFormValidTmp);
  };

  const onPostHandler = (event) => {
    event.preventDefault();

    const formData = {
      text: commentForm.text.value,
    };

    onCreateComment(postId, formData);
  };

  const formElementArray = [];
  for (const key in commentForm) {
    formElementArray.push({
      id: key,
      config: commentForm[key],
    });
  }

  let form = (
    <form className='form' onSubmit={onPostHandler}>
      {formElementArray.map((element) => {
        return (
          <Input
            key={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            value={element.config.value}
            valid={
              element.config.hasOwnProperty('valid')
                ? element.config.valid
                : true
            }
            touched={element.config.touched}
            changed={(event) => inputChangeHandler(event, element.id)}
          />
        );
      })}
      <Button btnClass={['Dark', 'Sm']} disabled={!isFormValid}>
        Submit
      </Button>
    </form>
  );

  let commentsSection = null;
  if (post && post.comments.length > 0) {
    commentsSection = post.comments.map((element) => {
      return <Comment key={element._id} postId={postId} content={element} />;
    });
  }

  return (
    <div className={styles.Posts}>
      <ButtonLink to='/posts' btnClass={['Light', 'Sm']}>
        Back To Posts
      </ButtonLink>
      <PostSummary content={post} readonly />
      <Separator text='Leave a Comment...' />
      {form}
      {commentsSection}
    </div>
  );
};

Post.propTypes = {
  auth: PropTypes.object.isRequired,
  onGetPost: PropTypes.func.isRequired,
  onCreateComment: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return { auth: state.auth, post: state.post };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetPost: (postId) => dispatch(actions.getPost(postId)),
    onCreateComment: (postId, formData) => dispatch(actions.createComment(postId, formData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);
