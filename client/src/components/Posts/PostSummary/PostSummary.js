import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../../../actions';

import styles from './PostSummary.module.css';
import Button from '../../UI/Button/Button';
import ButtonLink from '../../UI/ButtonLink/ButtonLink';

const PostSummary = ({
  auth,
  content,
  readonly,
  onAddLike,
  onRemoveLike,
  onDeletePost,
}) => {
  return (
    <div className={styles.Post}>
      <div className={styles.Avatar}>
        <Link to={`/profile/${content.user}`}>
          <img className={styles.Picture} src={content.avatar} alt='Profile' />
        </Link>
        <h4 className={styles.Name}>{content.name}</h4>
      </div>
      <div className={styles.Body}>
        <div className={styles.Text}>
          <p>{content.text}</p>
        </div>
        <div className={styles.Date}>
          <p>{`Posted on ${new Date(content.date)
            .toISOString()
            .slice(0, 10)}`}</p>
        </div>
        {readonly ? null : (
          <div className={styles.Actions}>
            <Button
              btnClass={['Light', 'Sm']}
              clicked={() => {
                onAddLike(content._id);
              }}
            >
              <i className='fas fa-thumbs-up'></i>
              {content.likes && content.likes.length > 0 ? (
                <span>{` (${content.likes.length})`}</span>
              ) : (
                <span>{` (0)`}</span>
              )}
            </Button>{' '}
            <Button
              btnClass={['Light', 'Sm']}
              clicked={() => {
                onRemoveLike(content._id);
              }}
            >
              <i className='fas fa-thumbs-down'></i>
              {content.unlikes && content.unlikes.length > 0 ? (
                <span>{` (${content.unlikes.length})`}</span>
              ) : (
                <span>{` (0)`}</span>
              )}
            </Button>{' '}
            <ButtonLink
              btnClass={['Primary', 'Sm']}
              to={`/post/${content._id}`}
            >
              Discussion
              {content.comments && content.comments.length > 0 ? (
                <span>{` (${content.comments.length})`}</span>
              ) : (
                <span>{` (0)`}</span>
              )}
            </ButtonLink>{' '}
            {auth.user._id === content.user ? (
              <Button
                btnClass={['Danger', 'Sm']}
                clicked={() => {
                  onDeletePost(content._id);
                }}
              >
                <i className='fas fa-trash-alt fa-sm'></i>
              </Button>
            ) : null}
          </div>
        ) }
      </div>
    </div>
  );
};

PostSummary.propTypes = {
  auth: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  readonly: PropTypes.bool.isRequired,
  onAddLike: PropTypes.func.isRequired,
  onRemoveLike: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddLike: (postId) => dispatch(actions.addLike(postId)),
    onRemoveLike: (postId) => dispatch(actions.removeLike(postId)),
    onDeletePost: (postId) => dispatch(actions.deletePost(postId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostSummary);
