import * as actionTypes from '../actions/types';

const initialState = {
  posts: [],
  post: null,
  loading: true,
  error: {},
};

const Post = (state = initialState, action) => {
  const { type, payload } = action;
  let newState = { ...state };

  switch (type) {
    case actionTypes.BEGIN_POSTS_ACTION:
      newState.loading = true;
      break;
    case actionTypes.GET_POSTS_SUCCESS:
      newState.posts = payload;
      break;
      case actionTypes.GET_POST_SUCCESS:
        newState.post = payload;
        break;
    case actionTypes.CREATE_COMMENT_FAIL:
    case actionTypes.DELETE_COMMENT_FAIL:
    case actionTypes.CREATE_POST_FAIL:
    case actionTypes.DELETE_POST_FAIL:
    case actionTypes.GET_POSTS_FAIL:
    case actionTypes.GET_POST_FAIL:
      newState.error = payload;
      break;
    case actionTypes.ADD_LIKE_SUCCESS:
    case actionTypes.REMOVE_LIKE_SUCCESS:
      newState.posts = newState.posts.map((p) => {
        if (p._id === payload.id) {
          p.likes = payload.likes;
        }
        return p;
      });
      break;
    case actionTypes.CREATE_POST_SUCCESS:
      newState.posts = [...state.posts];
      newState.posts.unshift(payload);
      break;
    case actionTypes.DELETE_POST_SUCCESS:
      newState.posts = newState.posts.filter((p) => p._id !== payload);
      break;
    case actionTypes.DELETE_COMMENT_SUCCESS:
    case actionTypes.CREATE_COMMENT_SUCCESS:
      console.log(payload)
      newState.post = {...state.post};
      newState.post.comments = payload;
      break;
    case actionTypes.END_POSTS_ACTION:
      newState.loading = false;
      break;
    default:
      break;
  }

  return newState;
};

export default Post;
