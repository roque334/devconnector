import * as actionTypes from '../actions/types';

const initialState = {
  token: localStorage.getItem('token'),
  isAuthenticated: null,
  loading: false,
  user: null,
};

const auth = (state = initialState, action) => {
  const { type, payload } = action;
  let newState = { ...state };
  switch (type) {
    case actionTypes.BEGIN_AUTH_ACTION:
      newState.loading = true;
      break;
    case actionTypes.GET_USER_SUCCESS:
      newState.isAuthenticated = true;
      newState.user = payload;
      break;
    case actionTypes.REGISTER_SUCCESS:
    case actionTypes.LOGIN_SUCCESS:
      localStorage.setItem('token', payload.token);
      newState.token = payload.token;
      newState.isAuthenticated = true;
      break;
    case actionTypes.GET_USER_FAIL:
    case actionTypes.REGISTER_FAIL:
    case actionTypes.LOGIN_FAIL:
    case actionTypes.LOGOUT:
      localStorage.removeItem('token');
      newState.token = null;
      newState.isAuthenticated = false;
      break;
    case actionTypes.END_AUTH_ACTION:
      newState.loading = false;
      break;
    default:
      break;
  }
  return newState;
};

export default auth;
