import * as actionTypes from '../actions/types';

const initialState = {
  profile: null,
  profiles: [],
  repos: [],
  loading: true,
  errors: {},
};

const profile = (state = initialState, action) => {
  const { type, payload } = action;
  const newState = { ...state };
  switch (type) {
    case actionTypes.BEGIN_PROFILE_ACTION:
      newState.loading = true;
      break;
    case actionTypes.CREATE_PROFILE_SUCCESS:
    case actionTypes.CREATE_EDUCATION_SUCCESS:
    case actionTypes.CREATE_EXPERIENCE_SUCCESS:
    case actionTypes.GET_PROFILE_SUCCESS:
    case actionTypes.DELETE_EXPERIENCE:
    case actionTypes.DELETE_EDUCATION:
      newState.profile = payload;
      break;
      case actionTypes.CREATE_PROFILE_FAIL:
      case actionTypes.CREATE_EDUCATION_FAIL:
      case actionTypes.CREATE_EXPERIENCE_FAIL:
    case actionTypes.GET_PROFILE_FAIL:
      newState.profile = null;
      newState.errors = payload;
      break;
    case actionTypes.GET_PROFILES_SUCCESS:
      newState.profiles = payload;
      break;
    case actionTypes.CLEAR_PROFILE:
      newState.profile = null;
      newState.repos = [];
      break;
    case actionTypes.END_PROFILE_ACTION:
      newState.loading = false;
      break;
    default:
      break;
  }
  return newState;
};

export default profile;
