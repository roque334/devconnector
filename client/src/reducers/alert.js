import { SET_ALERT, REMOVE_ALERT } from '../actions/types';

const initialState = [];

const alert = (state = initialState, action) => {
  const { type, payload } = action;
  let newState = [...state];
  switch (type) {
    case SET_ALERT:
      newState.push(payload);
      break;
    case REMOVE_ALERT:
        console.log(payload);
      newState = newState.filter((item) => item.id !== payload.id);
      break;
    default:
      break;
  }
  return newState;
};

export default alert;
