const express = require('express');

const { body, validationResult } = require('express-validator');

const auth = require('../../middleware/auth');
const Profile = require('../../models/Profile');
const User = require('../../models/User');
const Post = require('../../models/Post');
const { route } = require('./auth');
const router = express.Router();

// @route   GET api/profile/me
// @desc    Get current user's profile
// @access  Private
router.get('/me', auth, async (req, res) => {
  try {
    let profile = await Profile.findOne({ user: req.user.id }).populate(
      'user',
      ['name', 'avatar']
    );

    if (!profile) {
      return res
        .status(400)
        .json({ errors: [{ message: 'There is no profile for this user' }] });
    }

    return res.json(profile);
  } catch (err) {
    console.error(err.message);
    return res.status(500).send('Server Error');
  }
});

// @route   POST api/profile
// @desc    Create/Update a profile
// @access  Private
router.post(
  '/',
  [
    auth,
    [
      body('status', 'Status is required').notEmpty(),
      body('skills', 'Skills is required').notEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      company,
      website,
      location,
      bio,
      status,
      githubusername,
      skills,
      youtube,
      facebook,
      twitter,
      instagram,
      linkedin,
    } = req.body;
    
    console.log(skills);

    //Build profile object
    const profileFields = {};
    profileFields.user = req.user.id;
    if (company) profileFields.company = company;
    if (website) profileFields.website = website;
    if (location) profileFields.location = location;
    if (bio) profileFields.bio = bio;
    if (status) profileFields.status = status;
    if (githubusername) profileFields.githubusername = githubusername;
    if (skills) profileFields.skills = skills.map((s) => s.trim());

    //Build social object
    profileFields.social = {};
    if (youtube) profileFields.social.youtube = youtube;
    if (facebook) profileFields.social.facebook = facebook;
    if (twitter) profileFields.social.twitter = twitter;
    if (instagram) profileFields.social.instagram = instagram;
    if (linkedin) profileFields.social.linkedin = linkedin;

    try {
      let profile = await Profile.findOneAndUpdate(
        { user: req.user.id },
        { $set: profileFields },
        {
          new: true /*new to true to return the profile after the update SQL statement */,
          upsert: true /* Make this update into an upsert */,
        }
      );

      return res.json(profile);
    } catch (err) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route   GET api/profile
// @desc    Get all profiles
// @access  Public
router.get('/', async (req, res) => {
  try {
    let profiles = await Profile.find().populate('user', ['name', 'avatar']);
    return res.json(profiles);
  } catch (err) {
    console.error(err.message);
    return res.status(500).send('Server Error');
  }
});

// @route   GET api/profile/user/:userId
// @desc    Get profile by user ID
// @access  Public
router.get('/user/:userId', async (req, res) => {
  try {
    let profile = await Profile.findOne({
      user: req.params.userId,
    }).populate('user', ['name', 'avatar']);

    if (!profile) {
      return res.status(400).json({ errors: [{ message: 'Profile not found' }] });
    }

    return res.json(profile);
  } catch (err) {
    console.error(err.message);
    if (err.kind === 'ObjectId') {
      return res.status(400).json({ errors: [{ message: 'Profile not found' }] });
    }
    return res.status(500).send('Server Error');
  }
});

// @route   DELETE api/profile
// @desc    Delete profile, user & posts
// @access  Private
router.delete('/', auth, async (req, res) => {
  try {
    // @todo Remove user's posts
    await Post.deleteMany({ user: req.user.id });

    //Remove Profile
    await Profile.findOneAndRemove({ user: req.user.id });

    // Remove User
    await User.findOneAndRemove({ _id: req.user.id });

    return res.json({ message: 'User deleted' });
  } catch (err) {
    console.error(err.message);
    return res.status(500).send('Server Error');
  }
});

// @route PUT api/profile/experience
// @desc Add profile experience
// @access Private
router.put(
  '/experience',
  [
    auth,
    [
      body('title', 'Title is required').notEmpty(),
      body('company', 'Company is required').notEmpty(),
      body('from', 'From is required').isDate(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      title,
      company,
      location,
      from,
      to,
      current,
      description,
    } = req.body;
    //Build experience object
    const experienceFields = {};
    if (title) experienceFields.title = title;
    if (company) experienceFields.company = company;
    if (location) experienceFields.location = location;
    if (from) experienceFields.from = from;
    if (to) experienceFields.to = to;
    if (current) experienceFields.current = current;
    if (description) experienceFields.description = description;

    console.log("experienceFields",experienceFields);

    try {
      Profile.findOneAndUpdate(
        { user: req.user.id },
        { $push: { experience: experienceFields } },
        { new: true },
        (err, profile) => {
          if (err) {
            return res
              .status(400)
              .json({ errors: [{ message: 'Experience not found' }] });
          }
          return res.json(profile);
        }
      );
    } catch (error) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route PUT api/profile/experience/:expId
// @desc Update experience from profile
// @access Private
router.put(
  '/experience/:expId',
  [
    auth,
    [
      body('title', 'Title is required').notEmpty(),
      body('company', 'Company is required').notEmpty(),
      body('from', 'From is required').isDate(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      title,
      company,
      location,
      from,
      to,
      current,
      description,
    } = req.body;
    //Build experience object
    const experienceFields = {};
    experienceFields._id = req.params.expId;
    if (title) experienceFields.title = title;
    if (company) experienceFields.company = company;
    if (location) experienceFields.location = location;
    if (from) experienceFields.from = from;
    if (to) experienceFields.to = to;
    if (current) experienceFields.current = current;
    if (description) experienceFields.description = description;

    try {
      Profile.findOneAndUpdate(
        { user: req.user.id },
        { $set: { 'experience.$[element]': experienceFields } },
        {
          arrayFilters: [{ 'element._id': req.params.expId }],
          new: true,
        },
        (err, profile) => {
          if (err) {
            return res
              .status(400)
              .json({ errors: [{ message: 'Experience not found' }] });
          }
          return res.json(profile);
        }
      );
    } catch (err) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route DELETE api/profile/experience/:expId
// @desc Delete experience from profile
// @access Private
router.delete('/experience/:expId', auth, async (req, res) => {
  try {
    Profile.findOneAndUpdate(
      { user: req.user.id },
      { $pull: { experience: { _id: req.params.expId } } },
      { new: true },
      (err, profile) => {
        if (err) {
          return res
            .status(400)
            .json({ errors: [{ message: 'Experience not found' }] });
        }
        return res.json(profile);
      }
    );
  } catch (err) {
    console.error(err.message);
    return res.status(500).send('Server Error');
  }
});

// @route PUT api/profile/education
// @desc Add profile education
// @access Private
router.put(
  '/education',
  [
    auth,
    [
      body('institution', 'Institution is required').notEmpty(),
      body('degree', 'Degree is required').notEmpty(),
      body('fieldofstudy', 'Field of Study is required').notEmpty(),
      body('from', 'From is required').isDate(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      institution,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description,
    } = req.body;
    //Build experience object
    const educationFields = {};
    if (institution) educationFields.institution = institution;
    if (degree) educationFields.degree = degree;
    if (fieldofstudy) educationFields.fieldofstudy = fieldofstudy;
    if (from) educationFields.from = from;
    if (to) educationFields.to = to;
    if (current) educationFields.current = current;
    if (description) educationFields.description = description;

    try {
      Profile.findOneAndUpdate(
        { user: req.user.id },
        { $push: { education: educationFields } },
        { new: true },
        (err, profile) => {
          if (err) {
            return res
              .status(400)
              .json({ errors: [{ message: 'Education not found' }] });
          }
          return res.json(profile);
        }
      );
    } catch (error) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route PUT api/profile/education/:eduId
// @desc Update eduaction from profile
// @access Private
router.put(
  '/education/:eduId',
  [
    auth,
    [
      body('institution', 'Institution is required').notEmpty(),
      body('degree', 'Degree is required').notEmpty(),
      body('fieldofstudy', 'Field of Study is required').notEmpty(),
      body('from', 'From is required').isDate(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      institution,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description,
    } = req.body;
    //Build experience object
    const educationFields = {};
    educationFields._id = req.params.eduId;
    if (institution) educationFields.institution = institution;
    if (degree) educationFields.degree = degree;
    if (fieldofstudy) educationFields.fieldofstudy = fieldofstudy;
    if (from) educationFields.from = from;
    if (to) educationFields.to = to;
    if (current) educationFields.current = current;
    if (description) educationFields.description = description;

    try {
      Profile.findOneAndUpdate(
        { user: req.user.id },
        { $set: { 'education.$[element]': educationFields } },
        {
          arrayFilters: [{ 'element._id': req.params.eduId }],
          new: true,
        },
        (err, profile) => {
          if (err) {
            return res
              .status(400)
              .json({ errors: [{ message: 'Education not found' }] });
          }
          return res.json(profile);
        }
      );
    } catch (err) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route DELETE api/profile/education/:eduId
// @desc Delete education from profile
// @access Private
router.delete('/education/:eduId', auth, async (req, res) => {
  try {
    Profile.findOneAndUpdate(
      { user: req.user.id },
      { $pull: { education: { _id: req.params.eduId } } },
      { new: true },
      (err, profile) => {
        if (err) {
          return res
            .status(400)
            .json({ errors: [{ message: 'Education not found' }] });
        }
        return res.json(profile);
      }
    );
  } catch (err) {
    console.error(err.message);
    return res.status(500).send('Server Error');
  }
});

module.exports = router;
