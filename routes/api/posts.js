const express = require('express');
const { body, validationResult } = require('express-validator');

const auth = require('../../middleware/auth');
const User = require('../../models/User');
const Post = require('../../models/Post');
const router = express.Router();

// @route   POST api/posts
// @desc    Create a post
// @access Private
router.post(
  '/',
  [auth, [body('text', 'Text is required').notEmpty()]],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    }

    const { text } = req.body;
    try {
      const user = await User.findById(req.user.id).select('-password');

      const postFields = {};
      postFields.user = req.user.id;
      postFields.name = user.name;
      postFields.avatar = user.avatar;
      if (text) postFields.text = text;

      const post = new Post(postFields);
      await post.save();

      return res.json(post);
    } catch (err) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route   GET api/posts
// @desc    Get all posts
// @access  Private
router.get('/', auth, async (req, res) => {
  try {
    const posts = await Post.find().sort({ date: -1 }); //Most recent first
    return res.json(posts);
  } catch (err) {
    console.error(err.message);
    return res.status(500).send('Server Error');
  }
});

// @route   GET api/posts/:id
// @desc    Get post by id
// @access  Private
router.get('/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!post) {
      return res.status(404).json({ errors: [{ message: 'Post not found' }] });
    }

    return res.json(post);
  } catch (err) {
    console.error(err.message);

    if (err.kind === 'ObjectId') {
      return res.status(404).json({ errors: [{ message: 'Post not found' }] });
    }

    return res.status(500).send('Server Error');
  }
});

// @route   DELETE api/posts/:id
// @desc    Delete post by id
// @access  Private
router.delete('/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!post) {
      return res
        .status(404)
        .status.json({ errors: [{ message: 'Post not found' }] });
    }

    if (post.user.toString() !== req.user.id) {
      return res.status(401).json({ errors: [{ message: 'User not authorized' }] });
    }

    await post.remove();

    return res.json({ message: 'Post removed' });
  } catch (err) {
    console.error(err.message);

    if (err.kind === 'ObjectId') {
      return res.status(404).json({ errors: [{ message: 'Post not found' }] });
    }

    return res.status(500).send('Server Error');
  }
});

// @route   PUT api/posts/like/:id
// @desc    Like a post
// @access  Private
router.put('/like/:id', auth, async(req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!post) {
      return res
        .status(404)
        .status.json({ errors: [{ message: 'Post not found' }] });
    }

    //Check if the post has already been liked
    if (post.likes.filter(l => l.user.toString() === req.user.id).length > 0) {
      return res.status(400).json({ errors: [{ message: 'Post already liked' }] });
    }

    post.likes.unshift({user: req.user.id });
    await post.save();

    return res.json(post.likes);
  } catch (err) {
    console.error(err.message);

    if (err.kind === 'ObjectId') {
      return res.status(404).json({ errors: [{ message: 'Post not found' }] });
    }

    return res.status(500).send('Server Error');
  }
});

// @route   PUT api/posts/unlike/:id
// @desc    Like a post
// @access  Private
router.put('/unlike/:id', auth, async(req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!post) {
      return res
        .status(404)
        .status.json({ errors: [{ message: 'Post not found' }] });
    }

    //Check if the post has already been liked
    if (post.likes.filter(l => l.user.toString() === req.user.id).length === 0) {
      return res.status(400).json({ errors: [{ message: 'Post has not been liked' }] });
    }

    post.likes = post.likes.filter(l => l.user.toString() !== req.user.id);
    await post.save();

    return res.json(post.likes);
  } catch (err) {
    console.error(err.message);

    if (err.kind === 'ObjectId') {
      return res.status(404).json({ errors: [{ message: 'Post not found' }] });
    }

    return res.status(500).send('Server Error');
  }
});

// @route   POST api/posts/comment/:id
// @desc    Comment a post
// @access Private
router.post(
  '/comment/:id',
  [auth, [body('text', 'Text is required').notEmpty()]],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    }

    const { text } = req.body;
    try {
      const post = await Post.findById(req.params.id);

      if (!post) {
        return res
          .status(404)
          .status.json({ errors: [{ message: 'Post not found' }] });
      }

      const user = await User.findById(req.user.id).select('-password');

      const commentFields = {};
      commentFields.user = req.user.id;
      commentFields.name = user.name;
      commentFields.avatar = user.avatar;
      if (text) commentFields.text = text;

      post.comments.unshift(commentFields);
      await post.save();

      return res.json(post.comments);
    } catch (err) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route   DELETE api/posts/comment/:id/:commentId
// @desc    Delete a comment
// @access  Private
router.delete('/comment/:id/:commentId', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!post) {
      return res
        .status(404)
        .status.json({ errors: [{ message: 'Post not found' }] });
    }

    const comment = post.comments.find(c => c.id === req.params.commentId);

    if (!comment) {
      return res.status(404).json({ errors: [{ message: 'Comment does not exists' }] });
    }

    if (comment.user.toString() !== req.user.id) {
      return res.status(401).json({ errors: [{ message: 'User not authorized' }] });
    }

    post.comments = post.comments.filter(c => c.id !== req.params.commentId);

    await post.save();

    return res.json(post.comments);
  } catch (err) {
    console.error(err.message);

    if (err.kind === 'ObjectId') {
      return res.status(404).json({ errors: [{ message: 'Post not found' }] });
    }

    return res.status(500).send('Server Error');
  }
});

module.exports = router;
